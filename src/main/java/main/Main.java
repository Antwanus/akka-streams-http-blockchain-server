package main;

import streaming.Application;

public class Main {
   public static void main(String[] args) {
      Application app = new Application();
      app.runWithServer();
   }
}
