package streaming;

import akka.Done;
import akka.NotUsed;
import akka.actor.typed.ActorRef;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.AskPattern;
import akka.http.javadsl.ConnectionContext;
import akka.http.javadsl.Http;
import akka.http.javadsl.HttpsConnectionContext;
import akka.http.javadsl.ServerBinding;
import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.*;
import akka.http.javadsl.model.headers.RawHeader;
import akka.http.javadsl.model.ws.Message;
import akka.http.javadsl.model.ws.TextMessage;
import akka.http.javadsl.server.Route;
import akka.http.javadsl.unmarshalling.Unmarshaller;
import akka.stream.*;
import akka.stream.javadsl.*;
import akka.stream.typed.javadsl.ActorFlow;
import akka.util.ByteString;
import blockchain.MiningManagerBehavior;
import blockchain.BlockChainSystemBehavior;
import blockchain.TransactionManagerBehavior;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.*;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.CompletionStage;

import static akka.http.javadsl.server.Directives.*;
import static akka.http.javadsl.server.PathMatchers.*;

public class Application {
   private int transId = -1;
   private final Random random = new Random();
   private ActorSystem<MiningManagerBehavior.Command> actorSystem;
   private ActorRef<TransactionManagerBehavior.Command> txManager;

   public void runWithServer() {
      System.out.println("Starting actor-system...");
      actorSystem = ActorSystem.create(BlockChainSystemBehavior.create(), "blockChainMine");
      System.out.println("Searching for reference to TransactionManager...");
      CompletionStage<ActorRef<TransactionManagerBehavior.Command>> txManagerFuture = AskPattern.ask(
          actorSystem,                 BlockChainSystemBehavior.GetTxManagerActorRefCommand::new,
          Duration.ofSeconds(5),       actorSystem.scheduler()
      );

      txManagerFuture.whenComplete( (actorRef, throwable)-> {
         if (throwable != null) System.out.println("Something went wrong -.-' \n" +throwable);
         else {
            System.out.println("Reference to TransactionManager has been found!");
            txManager = actorRef;
         }
         startServer();
      });
   }
   private void startServer() {
      System.out.println("Starting server...");
      CompletionStage<ServerBinding> serverBindingCS = Http.get(actorSystem)
          .newServerAt("localhost", 443)
          .enableHttps(createHttpsContext())
          .bind( allRoutes());
      serverBindingCS.whenComplete( (serverBinding, throwable)-> {
         if (throwable != null) System.out.println("Something went wrong -.-' \n" +throwable);
         else System.out.println("Server is running!");
         System.out.println("Application.startServer(): 'Starting mining-system...");
         startMiningSystem();
      });
   }

   private Route allRoutes() {
      return pathPrefix("api", ()->
          concat(
              path("transaction", ()->
                  concat(  handleNewTxRoute(),handleGetTxStatusRoute() )),
              path("blockchain", this::getBlockChainRoute),
              path("monitor", this::monitorTxStatusRoute)
      ));
   }
   private Route monitorTxStatusRoute() { // monitor {id}
      Flow<Message, Message, NotUsed> webSocketHandlerFlow = Flow.of(Message.class)
          .map(msg -> {
             if (msg.isText()) {
                String rawMsg = msg.asTextMessage().getStrictText();
                String[] split = rawMsg.split(" ");
                if (split[0].equals("monitor")) {
                   int id = Integer.parseInt(split[1]);
                   Flow<Integer, TransactionStatus, NotUsed> txIdIntoTxStatusFlow = ActorFlow.ask(
                       txManager,
                       Duration.ofSeconds(5),
                       TransactionManagerBehavior.GetTransactionStatusCommand::new
                   );
                   Source<String, NotUsed> source = Source.single(id)
                       .via(txIdIntoTxStatusFlow)
//                       .map(txStatus -> txStatus.toString());
                       .map(Enum::toString);
                   return TextMessage.create(source);
                } else return TextMessage.create("ERROR: unknown msg request ("+split[0]+")\nTIP: try 'monitor'");
             } else return TextMessage.create("ERROR: you 're msg");

          });
      return get( ()-> handleWebSocketMessages(webSocketHandlerFlow));
   }
   private Route getBlockChainRoute() {
      return get(  () ->
          extractRequest(request -> {
             CompletionStage<BlockChain> blockChainCS = AskPattern.ask(
                  actorSystem,
                  BlockChainSystemBehavior.GetCopyOfBlockChain::new,
                  Duration.ofSeconds(5),
                  actorSystem.scheduler()
             );
             request.discardEntityBytes(actorSystem);
             return onComplete(blockChainCS, blockChainTry -> {
                try {
                   return complete( HttpEntities.create( ContentTypes.APPLICATION_JSON,
                                                         new ObjectMapper().writeValueAsString(blockChainTry.get())
                   ));
                } catch (JsonProcessingException e) {
                   e.printStackTrace();
                   return complete(StatusCodes.INTERNAL_SERVER_ERROR);
                }
          });
      }));
   }
   private Route handleGetTxStatusRoute(){
      return get( ()->
          extractRequest(request ->
          parameter("id", id -> {
             System.out.println("Retrieving status for transaction: " +id);
             int integerId = Integer.parseInt(id);
             Source<Integer, NotUsed> source = Source.single(integerId);
             Flow<Integer, TransactionStatus, NotUsed> txIdToTxStatusFlow = ActorFlow.ask(
                 txManager,
                 Duration.ofSeconds(5),
                 TransactionManagerBehavior.GetTransactionStatusCommand::new
             );
             Flow<TransactionStatus,ByteString,NotUsed> convertTxStatusToResponseFlow = Flow.of(TransactionStatus.class)
                 .map(status -> {
                    Map<String, Object> result = Map.of("transactionId", id,"txStatus", status);
                    String json = new ObjectMapper().writeValueAsString(result);
                    return ByteString.fromString(json);
                 });
             Source<ByteString, NotUsed> graph = source.via(txIdToTxStatusFlow).via(convertTxStatusToResponseFlow);

             request.discardEntityBytes(actorSystem);
             return complete(HttpEntities.create(ContentTypes.APPLICATION_JSON, graph));
          })));
   }
   private Route handleNewTxRoute() {
      Unmarshaller<HttpEntity, Transaction> txUnmarshaller = Jackson.unmarshaller(Transaction.class);
      List<HttpHeader> headers = List.of(
          RawHeader.create("Access-Control-Allow-Origin", "*"),
          RawHeader.create("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers"),
          RawHeader.create("Access-Control-Allow-Methods", "GET,POST,OPTIONS")
      );
      return concat(
          post( () ->
           respondWithHeaders(headers, () ->
              entity(txUnmarshaller, tx -> {          //because we extracted the entity, we don't have to discard bytes
                 Source<Transaction, NotUsed> source = Source.single(tx);
                 Flow<Transaction, Integer, NotUsed> receiveTxFlow = ActorFlow.ask(
                     txManager,
                     Duration.ofSeconds(5),
                     TransactionManagerBehavior.NewTransactionCommand::new);
                 Flow<Integer, ByteString, NotUsed> convertTxToResponseFlow = Flow.of(Integer.class)
                     .map(id -> {
                        Map<String, Integer> result = Map.of("transactionId", id);
                        String json = new ObjectMapper().writeValueAsString(result);
                        return ByteString.fromString(json);
                     });
                 Source<ByteString, NotUsed> graph = source.via(receiveTxFlow).via(convertTxToResponseFlow);

                 return complete(HttpEntities.create(ContentTypes.APPLICATION_JSON, graph));
          }))),
          options( () -> complete(HttpResponse.create().withStatus(200).withHeaders(headers)))
      );
   }

   private String getCertificatePassword() {
      try (
          InputStream inputStream = ClassLoader.getSystemResourceAsStream("security.properties");
      ) {
         Properties properties = new Properties();
         properties.load(inputStream);
         return properties.getProperty("certificatepassword");
      } catch (IOException e) {
         e.printStackTrace();
      }
      return null;
   }
   private HttpsConnectionContext createHttpsContext() {
      String password = getCertificatePassword();
      try (InputStream inputStream = ClassLoader.getSystemResourceAsStream("identity.p12");
      ){
         KeyStore ks = KeyStore.getInstance("PKCS12");
         ks.load(inputStream, password.toCharArray());
         KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
         kmf.init(ks, password.toCharArray());
         TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
         tmf.init(ks);
         SSLContext sslContext = SSLContext.getInstance("TLS");
         sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), new SecureRandom());
         return ConnectionContext.httpsServer(sslContext);
      } catch (KeyStoreException | IOException | CertificateException |
          NoSuchAlgorithmException | UnrecoverableKeyException | KeyManagementException e) {
         e.printStackTrace();
         throw new RuntimeException(e);
      }
   }

   Sink<Block, CompletionStage<Done>> sink = Sink.foreach(block -> {
      actorSystem.tell(new BlockChainSystemBehavior.AddBlockToBlockchain(block));
      CompletionStage<BlockChain> blockChainCS = AskPattern.ask(
          actorSystem,
          BlockChainSystemBehavior.GetCopyOfBlockChain::new,
          Duration.ofSeconds(5),
          actorSystem.scheduler()
      );
      blockChainCS.whenComplete((blockChain, throwable) -> {
         blockChain.printAndValidate();
      });
   });
   public void startMiningSystem() {
      Source<String, NotUsed> firstHashValue = Source.single("0");

      Flow<Block, HashResult, NotUsed> miningProcess = ActorFlow
          .ask(actorSystem, Duration.ofSeconds(30), (block, self) -> {
                 System.out.println("Starting to mine block= " +block);
                 return new MiningManagerBehavior.MineBlockCommand(block, self, 6);
          });

      Flow<String, Block, NotUsed> getNextMinableBlock = ActorFlow.ask(
          txManager, Duration.ofSeconds(999),
          (lastHash, blockActorRef) ->
              new TransactionManagerBehavior.GenerateBlockCommand(blockActorRef, lastHash)
      );

      RunnableGraph<NotUsed> miningGraph = RunnableGraph.fromGraph(GraphDSL.create(
          builder -> {
             UniformFanInShape<String, String> receiveHashes = builder.add(Merge.create(2));
             FlowShape<String, Block> applyLastHashToBlock = builder.add(getNextMinableBlock);

             UniformFanOutShape<Block, Block> broadcast = builder.add(Broadcast.create(2));
             FlowShape<Block, HashResult> mineBlock = builder.add(miningProcess);

             UniformFanOutShape<HashResult, HashResult> duplicateHashResult = builder.add(Broadcast.create(2));

             FanInShape2<Block, HashResult, Block> receiveHashResult =
                 builder.add(ZipWith.create((block, hashResult) -> {
                    block.setHash(hashResult.getHash());
                    block.setNonce(hashResult.getNonce());
                    return block;
                 }));

             SinkShape<Block> sinkShape = builder.add(sink);

             builder.from(builder.add(firstHashValue))
                 .viaFanIn(receiveHashes);
             builder.from(receiveHashes)
                 .via(applyLastHashToBlock);
             builder.from(applyLastHashToBlock.out())
                 .viaFanOut(broadcast);

             builder.from(broadcast)
                 .toInlet(receiveHashResult.in0());
             builder.from(broadcast)
                 .via(mineBlock)
                 .viaFanOut(duplicateHashResult)
                 .toInlet(receiveHashResult.in1());

             builder.from(duplicateHashResult)
                 .via(builder.add(Flow.of(HashResult.class).map(HashResult::getHash)))
                 .viaFanIn(receiveHashes);

             builder.from(receiveHashResult.out()).to(sinkShape);

             return ClosedShape.getInstance();
          })
      );

      Transaction firstTransaction = new Transaction(0, System.currentTimeMillis(), 0, 0);
      CompletionStage<Integer> firstTransactionCS = AskPattern.ask(
          txManager,
          ref -> new TransactionManagerBehavior.NewTransactionCommand(firstTransaction, ref),
          Duration.ofSeconds(5),
          actorSystem.scheduler());

      firstTransactionCS.whenComplete((transId,  throwable)-> {
         System.out.println("Starting mining graph");
         miningGraph.run(actorSystem);

      });
   }

}