package blockchain;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.SupervisorStrategy;
import akka.actor.typed.javadsl.*;
import model.Block;
import model.BlockChain;

public class BlockChainSystemBehavior extends AbstractBehavior<MiningManagerBehavior.Command> {

    private final PoolRouter<MiningManagerBehavior.Command> managerPoolRouter;
    private final ActorRef<MiningManagerBehavior.Command> managers;
    private final ActorRef<TransactionManagerBehavior.Command> txManager;
    private final BlockChain blockChain;

    public static class GetTxManagerActorRefCommand implements MiningManagerBehavior.Command {
        // an actorRef that expects to receive an actorRef
        ActorRef<ActorRef<TransactionManagerBehavior.Command>> replyTo;

        public GetTxManagerActorRefCommand(ActorRef<ActorRef<TransactionManagerBehavior.Command>> replyTo) {
            this.replyTo = replyTo;
        }

        public ActorRef<ActorRef<TransactionManagerBehavior.Command>> getReplyTo() { return replyTo; }
    }
    public static class AddBlockToBlockchain implements MiningManagerBehavior.Command {
        private final Block block;

        public AddBlockToBlockchain(Block block) {
            this.block = block;
        }

        public Block getBlock() { return block; }
    }
    public static class GetCopyOfBlockChain implements MiningManagerBehavior.Command {
        private final ActorRef<BlockChain> replyTo;

        public GetCopyOfBlockChain(ActorRef<BlockChain> replyTo) {
            this.replyTo = replyTo;
        }

        public ActorRef<BlockChain> getReplyTo() { return replyTo; }
    }

    private BlockChainSystemBehavior(ActorContext<MiningManagerBehavior.Command> context) {
        super(context);
        managerPoolRouter = Routers.pool(3,
                Behaviors.supervise(MiningManagerBehavior.create()).onFailure(SupervisorStrategy.restart()));
        managers = getContext().spawn(managerPoolRouter, "managerPool");
        txManager = getContext().spawn(TransactionManagerBehavior.create(), "txManager");
        blockChain = new BlockChain();
    }
    public static Behavior<MiningManagerBehavior.Command> create() {
        return Behaviors.setup(BlockChainSystemBehavior::new);
    }

    @Override
    public Receive<MiningManagerBehavior.Command> createReceive() {
        return newReceiveBuilder()
                .onMessage(GetTxManagerActorRefCommand.class, msg -> {
                    msg.getReplyTo().tell(txManager);
                    return Behaviors.same();
                })
                .onMessage(AddBlockToBlockchain.class, msg -> {
                    blockChain.addBlock(msg.getBlock());
                    return Behaviors.same();
                })
                .onMessage(GetCopyOfBlockChain.class, msg -> {
                    msg.getReplyTo().tell(blockChain);
                    return Behaviors.same();
                })
                .onAnyMessage(message -> {
                    managers.tell(message);
                    return Behaviors.same();
                })
                .build();
    }
}
